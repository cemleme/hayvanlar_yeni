package games.hebele.hayvanlari_ogreniyorum.Views;

import games.hebele.hayvanlari_ogreniyorum.HayvanlariOgreniyorum;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class SplashScreen implements Screen
{
	
	private FitViewport viewport;
    private SpriteBatch spriteBatch;
    private OrthographicCamera cam;
    
    private Texture splsh;
    private HayvanlariOgreniyorum myGame;
    private Stage stage;
    private Image splashImage;
    
	
	private float Virtual_Width=480;
	private float Virtual_Height=320;
	
    public SplashScreen(HayvanlariOgreniyorum g)
    {
            myGame = g;
            
            spriteBatch = new SpriteBatch();
     		viewport = new FitViewport(Virtual_Width, Virtual_Height);
    		stage = new Stage(viewport, spriteBatch);
    }

    @Override
    public void render(float delta)
    {
    		stage.act(delta);
    		Gdx.gl.glClearColor(1, 1, 1, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            
            spriteBatch.setProjectionMatrix(cam.combined);
            stage.draw();
            
            if(Gdx.input.justTouched())
                    myGame.goToMainMenuScreen();
    }
    
    @Override
    public void show()
    {		
    		cam = new OrthographicCamera();
			cam.setToOrtho(false, Virtual_Width, Virtual_Height);
    	
            splsh = new Texture(Gdx.files.internal("data/hebelegames_logo.png"));
            
            float logoW=480/3;
            float lohoH=logoW*splsh.getHeight()/splsh.getWidth();
            		
            splashImage = new Image(splsh);
            splashImage.setSize(logoW, lohoH);
            splashImage.setPosition((480-logoW)/2, (320-lohoH)/2);
            splashImage.getColor().a = 0f;

            // configure the fade-in/out effect on the splash image
            splashImage.addAction( sequence( fadeIn( 1f ), delay( 1f ), fadeOut( 1f ),
                new Action() {
                    @Override
                    public boolean act(
                        float delta )
                    {
                        // the last action will move to the next screen
                    	myGame.goToMainMenuScreen();
                        return true;
                    }
                } ) );

            
            stage.addActor( splashImage );
            
    }

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		spriteBatch.dispose();
		stage.dispose();
	}
}

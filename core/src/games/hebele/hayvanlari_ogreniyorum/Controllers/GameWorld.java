package games.hebele.hayvanlari_ogreniyorum.Controllers;

import games.hebele.hayvanlari_ogreniyorum.Assets;
import games.hebele.hayvanlari_ogreniyorum.Managers.LevelManager;
import games.hebele.hayvanlari_ogreniyorum.Managers.SoundManager;
import games.hebele.hayvanlari_ogreniyorum.Models.HayvanFactory;
import games.hebele.hayvanlari_ogreniyorum.Models.HayvanPrototype;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Json;

public class GameWorld {
	
	private TextureAtlas hayvanAtlas;
	
	public TextureRegion bg_landscape,bg_water,oyun_logo,button_sound;
	public TextureRegion btn_su_up,btn_su_down,btn_kara_up,btn_kara_down,btn_tum_up,btn_tum_down;
	
	
	public GameWorld(){
		
		hayvanAtlas = Assets.manager.get(Assets.hayvanPack, TextureAtlas.class);
		
		SoundManager.soundMap.put("sound_dogru", Assets.manager.get(Assets.soundDogru, Sound.class));
		SoundManager.soundMap.put("sound_aferin", Assets.manager.get(Assets.soundAferin, Sound.class));
		SoundManager.soundMap.put("sound_hayir_o", Assets.manager.get(Assets.soundHayirO, Sound.class));
		SoundManager.soundMap.put("sound_hangisi", Assets.manager.get(Assets.soundHangisi, Sound.class));
		
	     
		bg_landscape = hayvanAtlas.findRegion("bg_landscape");
		bg_water = hayvanAtlas.findRegion("bg_water");
		oyun_logo = hayvanAtlas.findRegion("hayvanlar_logo");
		button_sound = hayvanAtlas.findRegion("sound");
		
		btn_su_up = hayvanAtlas.findRegion("btn_su_up");
		btn_su_down = hayvanAtlas.findRegion("btn_su_down");
		btn_kara_up = hayvanAtlas.findRegion("btn_kara_up");
		btn_kara_down = hayvanAtlas.findRegion("btn_kara_down");
		btn_tum_up = hayvanAtlas.findRegion("btn_tum_up");
		btn_tum_down = hayvanAtlas.findRegion("btn_tum_down");	

		initAnimals();	
	}
	
	private void initAnimals(){
		Json json = new Json();
		json.setElementType(HayvanFactory.class, "hayvanlar", HayvanPrototype.class);
		HayvanFactory hf = json.fromJson(HayvanFactory.class, Gdx.files.internal("hayvanlar.json"));
		hf.processData(hayvanAtlas);
	}
	

	public TextureRegion getlevelBackground(){
        switch (LevelManager.currentState) {
        case KARA:
        default:
    		return bg_landscape;
		case SU:
			return bg_water;
        }
	}

	
	public void dispose(){
	}
}

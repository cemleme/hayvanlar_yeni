package games.hebele.hayvanlari_ogreniyorum;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class Assets {
	
	public static AssetManager manager;
	public static String guiPack,hayvanPack,soundHangisi,soundDogru,soundAferin,soundHayirO;
	
	public static void init(){
		manager = new AssetManager();
		guiPack ="data/gui.atlas";
		hayvanPack ="data/hayvanpack.pack";
		soundHangisi ="sound/hangisi.mp3";
		soundDogru ="sound/dogru.mp3";
		soundAferin ="sound/aferin.mp3";
		soundHayirO ="sound/hayir_o.mp3";		
	}
	
	
	public static void load(){
		manager.load(guiPack, TextureAtlas.class);
		manager.load(hayvanPack, TextureAtlas.class);
		
		manager.load(soundHangisi, Sound.class);
		manager.load(soundDogru, Sound.class);
		manager.load(soundAferin, Sound.class);
		manager.load(soundHayirO, Sound.class);
	}
	
	public static void dispose(){
		manager.dispose();
	}
}

package games.hebele.hayvanlari_ogreniyorum.Models;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class animalActor extends Actor {
	TextureRegion textureRegion;
    public boolean started = false;
    private String name;
    
    private float actorScale = 0.9f;
    
    public animalActor(String name){
    	this.name=name;
    }
    
	public animalActor(TextureRegion textureRegion,Vector2 position, String name){
		this.textureRegion=textureRegion;
		this.name=name;
		
		this.setX(position.x);
		this.setY(position.y);
		this.setHeight(textureRegion.getRegionHeight());
		this.setWidth(textureRegion.getRegionWidth());
		this.setScale(actorScale);
		
        setBounds(getX(),getY(),getWidth(),getHeight());
	}
	
	public void dogruCevapAksiyonlari() {
    	toFront();
    	addAction(sequence(
    				parallel(
    					moveTo(150, 10, 2), 
    					scaleTo(1.5f,1.5f,2)
    	)));
	}
	
	public void yanlisCevapAksiyonlari(){
        addAction(sequence(
        		rotateTo(10f, 0.2f),
        		rotateTo(-10f, 0.2f),
        		rotateTo(10f, 0.2f),
        		rotateTo(-10f, 0.2f),
        		rotateTo(0f, 0.2f)
        ));
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
	    batch.draw(textureRegion,this.getX(),getY(),this.getOriginX(),this.getOriginY(),this.getWidth(),
	            this.getHeight(),this.getScaleX(), this.getScaleY(),this.getRotation());
	}
	

    @Override
    public void act(float delta){
    	super.act(delta);
    }
    
    public void start(){
    	started=true;
    }
    
    public void setTexture(TextureRegion textureRegion){
    	this.textureRegion=textureRegion;
    }
    
    public void setName(String name){
		this.name=name;
    }
    
    public String getName(){
    	return name;
    }

}

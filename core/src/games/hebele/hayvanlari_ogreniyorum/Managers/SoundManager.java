package games.hebele.hayvanlari_ogreniyorum.Managers;

import java.util.HashMap;
import java.util.Random;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class SoundManager {
	
	public static HashMap<String,Sound> soundMap;

	public static void init(){
		soundMap = new HashMap<String,Sound>();
	}
	
	public static void playSoundHangisi(final String name){
		soundMap.get("sound_hangisi").play();
		
    	Timer.schedule(new Task(){
 		   @Override
 		   public void run(){
 			  playSoundByName(name);
 		      }
 		   }, 0.6f);
    	
    	/*
    	final String soundName=name;
    	Thread t = new Thread(new Runnable() {
    		String n=soundName;
            public void run()
            {
    	    	try {
					Thread.sleep(600);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
    	    	playSoundByName(n);
            }
    	});
    	t.start();  */
	}
	
	public static void playSoundDogru(){
		int i=new Random().nextInt(2);
		try {
			if(i==1) soundMap.get("sound_dogru").play();
			else soundMap.get("sound_aferin").play();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
    public static void playSoundYanlisHayvan(final String name){
    	playSoundByName("hayir_o");
    	
    	Timer.schedule(new Task(){
    		   @Override
    		   public void run(){
    			  playSoundByName(name);
    		      }
    		   }, 1.0f);
    	
    	/*
    	Thread t = new Thread(new Runnable() {
            public void run()
            {
    	    	try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    	    	playSoundByName(name.toLowerCase());
            }
    	});
    	t.start();  */
    }

	public static void playSoundByName(String name){
		try {
			soundMap.get("sound_"+name.toLowerCase().replaceAll("\\s","")).play();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
